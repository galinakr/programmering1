import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * A class which registers and administrates TrainDepartures.
 * ArrayList
 *
 * @version 1.0
 * @author Galina Kristensen
 */

public class DepartureRegister {
  ArrayList<TrainDeparture> departureRegister = new ArrayList<>();

  /**
   * First method to add a new TrainDeparture to the register.
   * First the method checks if an object with the given trainNumber already exists
   * The method takes inn the parameters corresponding to TrainDeparture's first constructor
   * and then creates a new object of the TrainDeparture class
   * before adding it to the register ArrayList
   *
   * @param departureTime time, has to be a positive number
   * @param line Line, the route of the train
   * @param trainNumber Train number, a unique ID (within a 24-hour window)
   * @param track Track from which the train departs from
   * @param destination Of the departing train
   * @param delay The amount of delay from departure time
   *
   * @return returns false if, by using the findDeparture()-method,
   *                       the TrainDeparture already exists
   *         returns true if the TrainDeparture doesn't already exist,
   *                      the TrainDeparture object is created and added to the register
   */
  public boolean addDeparture(LocalTime departureTime, String line, int trainNumber, int track,
                              String destination, Duration delay) {
    if (findDeparture(trainNumber) == null) {
      TrainDeparture newDep = new TrainDeparture(departureTime, line, trainNumber,
              track, destination, delay);
      departureRegister.add(newDep);
      return true;
    }
    return false;
  }

  /**
   * Second method to add a new TrainDeparture to the register.
   * First the method checks if an object with the given trainNumber already exists
   * The method takes inn the parameters corresponding to TrainDeparture's second constructor
   * and then creates a new object of the TrainDeparture class
   * before adding it to the register ArrayList
   *
   * @param departureTime time, has to be a positive number
   * @param line Line, the route of the train
   * @param trainNumber Train number, a unique ID (within a 24-hour window)
   * @param destination Of the departing train
   * @param delay The amount of delay from departure time
   *
   * @return returns false if, by using the findDeparture()-method,
   *                       the TrainDeparture already exists
   *         returns true if the TrainDeparture doesn't already exist,
   *                      the TrainDeparture object is created and added to the register
   */
  public boolean addDeparture(LocalTime departureTime, String line, int trainNumber,
                              String destination, Duration delay) {
    if (findDeparture(trainNumber) == null) {
      TrainDeparture newDep = new TrainDeparture(departureTime, line,
              trainNumber, destination, delay);
      departureRegister.add(newDep);
      return true;
    }
    return false;
  }

  /**
   * Third method to add a new TrainDeparture to the register.
   * First the method checks if an object with the given trainNumber already exists
   * The method takes inn the parameters corresponding to TrainDeparture's third constructor
   * and then creates a new object of the TrainDeparture class
   * before adding it to the register ArrayList
   *
   * @param departureTime time, has to be a positive number
   * @param line Line, the route of the train
   * @param trainNumber Train number, a unique ID (within a 24-hour window)
   * @param track Track from which the train departs from
   * @param destination Of the departing train
   *
   * @return returns false if, by using the findDeparture()-method,
   *                       the TrainDeparture already exists
   *         returns true if the TrainDeparture doesn't already exist,
   *                     the TrainDeparture object is created and added to the register
   */
  public boolean addDeparture(LocalTime departureTime, String line, int trainNumber,
                              int track, String destination) {
    if (findDeparture(trainNumber) == null) {
      TrainDeparture newDep = new TrainDeparture(departureTime, line, trainNumber,
              track, destination);
      departureRegister.add(newDep);
      return true;
    }
    return false;
  }

  /**
   * Fourth method to add a new TrainDeparture to the register.
   * First the method checks if an object with the given trainNumber already exists
   * The method takes inn the parameters corresponding to TrainDeparture's fourth constructor
   * and then creates a new object of the TrainDeparture class
   * before adding it to the register ArrayList
   *
   * @param departureTime time, has to be a positive number
   * @param line Line, the route of the train
   * @param trainNumber Train number, a unique ID (within a 24-hour window)
   * @param destination Of the departing train
   *
   * @return returns false if, by using the findDeparture()-method,
   *                       the TrainDeparture already exists
   *         returns true if the TrainDeparture doesn't already exist,
   *                     the TrainDeparture object is created and added to the register
   */
  public boolean addDeparture(LocalTime departureTime, String line, int trainNumber, String destination) {
    if (findDeparture(trainNumber) == null) {
      TrainDeparture newDep = new TrainDeparture(departureTime, line, trainNumber, destination);
      departureRegister.add(newDep);
      return true;
    }
    return false;
  }

  /**
   * Method for finding a specified TrainDeparture object given its unique trainNumber.
   * With the desired trainNumber, the method goes through the ArrayList until it finds a match.
   * trainNumber IDs are unique, which is why the first match is enough.
   *
   * @param trainNumber The given train number to find a specific departure
   * @return Returns the TrainDeparture class found by trainNumber
   *         Returns null if no TrainDeparture object is found with the given trainNumber
   */
  public TrainDeparture findDeparture(int trainNumber) {
    for (TrainDeparture t : departureRegister) {
      if (t.getTrainNumber() == trainNumber) {
        return t;
      }
    }
    return null;
  }

  /**
   * Method for finding all TrainDeparture objects with a given destination.
   * First the method creates a new ArrayList, and shallow copies the register
   * Then a StringBuilder object is created, appended with the beginnings of a timetable
   * The ArrayList is filtered to only include TrainDepartures with the correct destination
   * The method then checks if the list is empty
   * The list is sorted by departure time and then each item is added to the StringBuilder
   *
   * @param destination The desired destination
   * @return Returns null if the list is empty, meaning no TrainDepartures have
   *                      the desired destination
   *         Returns the StringBuilder with the TrainDepartures with the desired destination
   */
  public String findDestination(String destination) {
    ArrayList<TrainDeparture> departures = new ArrayList<>();

    departures = departureRegister;
    StringBuilder s = new StringBuilder();
    s.append("""
              Time  | Line |Train Nr.| Destination    | Delay | New Time | Track |
            """);

    departures.stream()
            .filter((TrainDeparture t) -> t.getDestination().equalsIgnoreCase(destination))
            .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
            .forEach(s::append);

    if (departures.isEmpty()) {
      return null;
    }
    return s.toString();
  }

  /**
   * Method for removing a TrainDeparture from the register.
   * The method first checks if the register contains the TrainDeparture object
   * If it does, the method removes it. It then checks if the removal successful
   *
   * @param t The desired TrainDeparture object to be removed
   * @return Returns 1 if the removal of the TrainDeparture was successful
   *         Returns 0 if the removal was not successful
   *         Returns -1 if the register already does not contain the TrainDeparture
   */
  public int removeDeparture(TrainDeparture t) {
    if (departureRegister.contains(t)) {
      departureRegister.remove(t);

      if (!departureRegister.contains(t)) {
        return 1;
      }
      return 0;
    }
    return -1;
  }

  /**
   * Method for updating the Table.
   * The method goes through the register and checks if the new departure time is
   * less than the given time
   * If it is, the TrainDeparture class is removed
   *
   * @param time The given time to update the table with
   */
  public ArrayList<TrainDeparture> updateTable(LocalTime time) {
    ArrayList<TrainDeparture> dep = new ArrayList<>();

    for (TrainDeparture t : departureRegister) {
      if (!t.newTime().isBefore(time)) {
        dep.add(t);
      }
    }

    return dep;
  }

  /**
   * Method for viewing the timetable.
   * First the timetable is updated with the given time
   * Then a StringBuilder class is created with the beginnings of the timetable
   * Each TrainDeparture is sorted then added to the StringBuilder object
   * The StringBuilder is appended with the end of the timetable
   *
   * @param time The given time to update the timetable with
   * @return The StringBuilder with the finished timetable
   */
  public String viewTable(LocalTime time) {

    ArrayList<TrainDeparture> dep = updateTable(time);

    StringBuilder s = new StringBuilder();
    s.append("Current time: ").append(time).append("\n");
    s.append("""
             Time  | Line |Train Nr.| Destination    | Delay | New Time | Track |
            """);

    dep.stream()
            .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
            .forEach(s::append);
    return s.toString();
  }
}