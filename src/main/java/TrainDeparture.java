import java.time.LocalTime;
import java.time.Duration;
import java.time.format.DateTimeFormatter;

/**
 * A class representing a train departure.
 *
 * @version 1.0
 * @author Galina Kristensen
 */

public class TrainDeparture {
  private final LocalTime departureTime; //read-only, hh:mm
  private final String line; //read-only, the route the train is on, the line
  private final int trainNumber; //read-only, a unique (within a 24-hour window) train ID
  private int track; //Changeable, the track the train is assigned to for the station
  private final String destination; //read-only, the trains final stop, its destination
  private Duration delay; //Changeable, the amount of delay if the departure is delayed, hh:mm

  /**
   * First constructor for a TrainDeparture.
   *
   * @param departureTime time, has to be a positive number
   * @param line Line, the route of the train
   * @param trainNumber Train number, a unique ID (within a 24-hour window)
   * @param track Track from which the train departs from
   * @param destination Of the departing train
   * @param delay The amount of delay from departure time
   */
  TrainDeparture(LocalTime departureTime, String line,
                 int trainNumber, int track, String destination, Duration delay) {
    if (trainNumber < 0 || track < 0) {
      throw new IllegalArgumentException("""
      Departure time, train number, delay and track cannot be negative numbers
      """);
    }
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = delay;
  }

  /**
   * Second constructor for a TrainDeparture.
   * In the case track is unassigned, it is set to -1
   *
   * @param departureTime time, has to be a positive number
   * @param line Line, the route of the train
   * @param trainNumber Train number, a unique ID (within a 24-hour window)
   * @param destination Of the departing train
   * @param delay The amount of delay from departure time
   */
  TrainDeparture(LocalTime departureTime, String line, int trainNumber, String destination, Duration delay) {
    if (trainNumber < 0) {
      throw new IllegalArgumentException("""
      Departure time, train number and delay cannot be negative numbers
      """);
    }
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    track = -1;
    this.delay = delay;
  }

  /**
   * Third constructor for a TrainDeparture.
   * In case there is no delay
   *
   * @param departureTime time, has to be a positive number
   * @param line Line, the route of the train
   * @param trainNumber Train number, a unique ID (within a 24-hour window)
   * @param track Track from which the train departs from
   * @param destination Of the departing train
   */
  TrainDeparture(LocalTime departureTime, String line, int trainNumber, int track, String destination) {
    if (trainNumber < 0 || track < 0) {
      throw new IllegalArgumentException("""
      Departure time, train number and track cannot be negative numbers
      """);
    }
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    delay = Duration.ofMinutes(0);
  }

  /**
   * Last & fourth constructor for a TrainDeparture.
   * In case track and delay are unassigned, in which case track is assigned to -1 and delay is 0
   *
   * @param departureTime time, has to be a positive number
   * @param line Line, the route of the train
   * @param trainNumber Train number, a unique ID (within a 24-hour window)
   * @param destination Of the departing train
   */
  TrainDeparture(LocalTime departureTime, String line, int trainNumber, String destination) {
    if (trainNumber < 0) {
      throw new IllegalArgumentException("""
      Departure time and train number cannot be negative numbers
      """);
    }

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    track = -1;
    delay = Duration.ofMinutes(0);
  }

  /**
   * Accessor method: get departureTime.
   *
   * @return departureTime, when the train departs
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Accessor method: get line.
   *
   * @return line
   */
  public String getLine() {
    return line;
  }

  /**
   * Accessor method: get trainNumber.
   *
   * @return trainNumber
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Accessor method: get track.
   *
   * @return track
   */
  public int getTrack() {
    return track;
  }

  /**
   * Accessor method: get destination.
   *
   * @return destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Accessor method: get delay.
   *
   * @return delay
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Method which calculates the new time based on original departure time and delay.
   * <p>
   *     First the method adds the delayed hours to departure hours
   *     Next the method adds the delayed minutes to departure minutes
   *     Afterward the methods sets up a factor variable to prepare for the upcoming if-statements
   *     The factor represents whether the newMinutes are divisible by 60, and how many times
   * </p>
   * <p>
   *     If the resulting newMinutes exceeds 60, the factor is added to newHours
   *     Then the exceeding minutes are subtracted
   *     (the factor is multiplied with 60 to achieve this)
   * </p>
   * <p>
   *     Next the method checks if the hours exceeds 23
   *     In which case the method subtracts 24 from newHours
   *     (Midnight becomes 00:00, not 24:00, to better signify whether it's a new day)
   * </p>
   *
   * @return newTime as int
   */
  public LocalTime newTime() {
    return departureTime.plus(delay);
  }

  /**
   * Mutator method: changes track number.
   *
   * @param newTrack the new track
   * @return Returns true if change in track is successful
   *         If the new track is less than -1 (-1 representing unassigned), then returns false
   */
  public boolean setTrack(int newTrack) {
    if (newTrack < -1) {
      return false;
    }

    track = newTrack;
    return true;
  }

  /**
   * Mutator method: changes delay.
   *
   * @param newDelay the new delay
   * @return Returns true if change in delay is successful
   *         If the new delay is a negative number, then returns false
   */
  public boolean setDelay(Duration newDelay) {
    delay = newDelay;
    return true;
  }

  /**
   * Method for quickly identifying what information the object of the class TrainDeparture has.
   * Used in toSting since the resulting string depends on the information
   *
   * @return 1 if the object has an unassigned track but has no delay
   *         2 if the object has an unassigned track and has delay
   *         3 if the object has an assigned track and no delay
   *         4 if the object has an assigned track but has delay
   */
  public int numberForSwitch() {
    if (track == -1) {
      if (delay == Duration.ofMinutes(0)) {
        return 1; // no track and no delay
      } else {
        return 2; // no track and yes delay
      }
    } else {
      if (delay == Duration.ofMinutes(0)) {
        return 3; // yes track and no delay
      } else {
        return 4; // yes track and yes delay
      }
    }
  }

  /**
   * Method to check if two objects equal each other.
   * Identical: Both objects reference the same TrainDeparture
   * Content: Both objects have the same trainNumber
   *
   * @param o The object we are checking equality with
   * @return returns true if the objects are equal, returns false if they are not
   */
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o instanceof TrainDeparture) {
      TrainDeparture temporary = (TrainDeparture) o;
      return this.getTrainNumber() == temporary.getTrainNumber();
    }
    return false;
  }

  /**
   * toString method for the TrainDeparture class.
   *
   * @return String in the given format (if certain areas are empty, they are skipped):
   *        "(departure time), (line), (train number), (track), (destination), (delay and new time)"
   *        Example:
   *        Departure | line | train nr. | track | destination | delay and new time
   *        13:16    | L1   | 36       | 4     | Oslo   | The train is 0:30 delayed, new time: 13:46
   */
  public String toString() {
    String format;
    LocalTime del = LocalTime.of(delay.toHoursPart(), delay.toMinutesPart());

    return switch (numberForSwitch()) {
      case 1 -> {
        format = "%7s | %5s | %7s | %14s | %10s | %10s | %10s |\n";
        yield String.format(format, departureTime.toString(), line,
                trainNumber, destination, " ", " ", " ");
      }
      case 2 -> {
        format = "%7s | %5s | %7s | %14s | %5s | %8s | %10s |\n";
        yield String.format(format, departureTime.toString(), line, trainNumber, destination,
                del.format(DateTimeFormatter.ofPattern("h:m")), newTime().toString(), " ");
      }
      case 3 -> {
        format = "%7s | %5s | %7s | %14s | %10s | %10s | %5s |\n";
        yield String.format(format, departureTime.toString(), line, trainNumber, destination,
                " ", " ", track);
      }
      default -> {
        format = "%7s | %5s | %7s | %14s | %5s | %8s | %5s |\n";
        yield String.format(format, departureTime.toString(), line, trainNumber, destination,
                del.format(DateTimeFormatter.ofPattern("h:m")), newTime().toString(), track);
      }
    };
  }
}