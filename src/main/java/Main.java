/**
 * The class taking in user input.
 *
 * @version 1.0
 * @author Galina Kristensen
 */

public class Main {

  /**
   * The main application.
   *
   * @param args args
   */
  public static void main(String[] args) {
    UserInterface usIn = new UserInterface();
    usIn.init();
    usIn.start();
  }
}
