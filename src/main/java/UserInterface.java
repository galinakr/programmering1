import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * A class to facilitate communication between the class DepartureRegister and the user.
 *
 * @version 1.0
 * @author Galina Kristensen
 */

public class UserInterface {
  private DepartureRegister dr;
  private LocalTime time = LocalTime.of(0, 0);

  /**
   * Method to initializes the application.
   * Instantiates the register class.
   */
  public void init() {
    this.dr = new DepartureRegister();
  }

  /**
   * Method to starts the application.
   * Creates 4 departures through DepartureRegister
   * Starts runMenu
   * When runMenu is done, exit
   */
  public void start() {
    dr.addDeparture(LocalTime.of(13, 16), "L1", 101, 4, "Spikkestad", Duration.ofMinutes(5));
    dr.addDeparture(LocalTime.of(13, 18), "L1", 11, 6, "Lillestrøm");
    dr.addDeparture(LocalTime.of(16, 29), "L12", 201, "Eidsvoll");
    dr.addDeparture(LocalTime.of(16, 30), "L12", 21, "Kongsberg", Duration.ofMinutes(150));


    runMenu();

    exit();
  }

  /**
   * Method to run the menu selection.
   * Receives menu choice selection to menuChoice method.
   */
  public void runMenu() {

    int mc = -1;

    while (mc == -1) {
      mc = menuChoice();
    }

    while (mc != 0) {
      switch (mc) {
        case 1:
          viewTimeTable();
          break;
        case 2:
          addDeparture();
          break;
        case 3:
          assignTrack();
          break;
        case 4:
          addDelay();
          break;
        case 5:
          findDeparture();
          break;
        case 6:
          findDestination();
          break;
        case 7:
          updateClock();
          break;
        default:
      }
      mc = menuChoice();
    }
  }

  /**
   * Method for chosen menu option.
   *
   * @return menuChoice, the int representing the chosen option
   */
  public int menuChoice() {
    String txtChoice = showInputDialog("""
            Train Departure System v1.0
            ---
            1: View timetable
            2: Add new departure
            3: Assign track based on train number
            4: Add delay based on train number
            5: Search for departure based on train number
            6: Search for departures based on destination
            7: Update clock
            ------
            0: Exit
            """);

    if (!txtChoice.isBlank()) {
      try {
        return Integer.parseInt(txtChoice);
      } catch (Exception e) {
        showMessageDialog(null, "Choice must be a number");
        return -1;
      }
    } else {
      return 0;
    }
  }

  /**
   * Method for ending the application.
   */
  public void exit() {
    showMessageDialog(null, "Exited");
  }

  /**
   * Method to view the timetable.
   * Shows an overview of all departures.
   */
  public void viewTimeTable() {
    showMessageDialog(null, dr.viewTable(time));
  }

  /**
   * Method to add a new departure through the register.
   * Constructor instantiated with depends on information given.
   */
  public void addDeparture() {
    String depHourTxt = showInputDialog("Hour of departure:");
    int depHour;
    while (depHourTxt.isEmpty()) {
      depHourTxt = showInputDialog("Hour of departure cannot be empty");
    }
    try {
      depHour = Integer.parseInt(depHourTxt);
    } catch (Exception e) {
      depHourTxt = showInputDialog("Hour of departure must be a positive whole number under 24");
    } finally {
      depHour = Integer.parseInt(depHourTxt);
    }
    while (depHour <= 0 || depHour >= 23) {
      depHourTxt = showInputDialog("Hour of departure must be between 0 and 24");
      depHour = Integer.parseInt(depHourTxt);
    }

    String depMinTxt = showInputDialog("Minute of departure");
    int depMin;
    while (depMinTxt.isEmpty()) {
      depMinTxt = showInputDialog("Minute of departure cannot be empty");
    }
    try {
      depMin = Integer.parseInt(depMinTxt);
    } catch (Exception e) {
      depMinTxt = showInputDialog("Minute of departure must be a positive whole number under 60");
    } finally {
      depMin = Integer.parseInt(depMinTxt);
    }
    while (depMin <= 0 || depMin >= 59) {
      depMinTxt = showInputDialog("Minute of departure must be between 0 and 60");
      depMin = Integer.parseInt(depMinTxt);
    }

    String line = showInputDialog("Line:");
    while (line.isEmpty()) {
      line = showInputDialog("Line cannot be empty:");
    }

    String trainNumTxt = showInputDialog("Train number:");
    int trainNum;
    while (trainNumTxt.isEmpty()) {
      trainNumTxt = showInputDialog("Train number cannot be empty");
    }
    try {
      trainNum = Integer.parseInt(trainNumTxt);
    } catch (Exception e) {
      trainNumTxt = showInputDialog("Train number must be a positive whole number");
    } finally {
      trainNum = Integer.parseInt(trainNumTxt);
    }

    String trackTxt = showInputDialog("Track (optional):");
    int track;
    if (trackTxt.isEmpty()) {
      track = -1;
    } else {
      try {
        track = Integer.parseInt(trackTxt);
      } catch (Exception e) {
        trackTxt = showInputDialog("Track must be a positive whole number");
      } finally {
        track = Integer.parseInt(trackTxt);
      }
    }

    String destination = showInputDialog("Destination:");
    while (destination.isEmpty()) {
      destination = showInputDialog("Destination cannot be empty");
    }

    String delayTxt = showInputDialog("Total delay in minutes (optional):");
    int delay;
    if (delayTxt.isEmpty()) {
      delay = 0;
    } else {
      try {
        delay = Integer.parseInt(delayTxt);
      } catch (Exception e) {
        delayTxt = showInputDialog("Delay must be a positive whole number");
      } finally {
        delay = Integer.parseInt(delayTxt);
      }
    }

    if (track == -1) {
      if (delay == 0) {
        dr.addDeparture(LocalTime.of(depHour, depMin), line, trainNum, destination);
      } else {
        dr.addDeparture(LocalTime.of(depHour, depMin), line, trainNum, destination,
                Duration.ofMinutes(delay));
      }
    } else {
      if (delay == 0) {
        dr.addDeparture(LocalTime.of(depHour, depMin), line, trainNum, track, destination);
      } else {
        dr.addDeparture(LocalTime.of(depHour, depMin), line, trainNum, track, destination,
                Duration.ofMinutes(delay));
      }
    }

  }

  /**
   * Method for assigning or unassigning a track for a given Departure.
   */
  public void assignTrack() {
    String trainNumTxt = showInputDialog("Find desired departure by train number:");
    int trainNum;
    while (trainNumTxt.isEmpty()) {
      trainNumTxt = showInputDialog("Train number cannot be empty");
    }
    try {
      trainNum = Integer.parseInt(trainNumTxt);
    } catch (Exception e) {
      trainNumTxt = showInputDialog("Train number must be a positive whole number");
    } finally {
      trainNum = Integer.parseInt(trainNumTxt);
    }

    String trackTxt = showInputDialog("Track (leave blank to remove):");
    int track;
    if (trackTxt.isEmpty()) {
      track = -1;
    } else {
      try {
        track = Integer.parseInt(trackTxt);
      } catch (Exception e) {
        trackTxt = showInputDialog("Track must be a positive whole number");
      } finally {
        track = Integer.parseInt(trackTxt);
      }
    }

    dr.findDeparture(trainNum).setTrack(track);
    if (dr.findDeparture(trainNum).getTrack() == -1) {
      showMessageDialog(null, "Track assignment removed.");
    } else {
      showMessageDialog(null, "Track now assigned as " + dr.findDeparture(trainNum).getTrack());
    }
  }

  /**
   * Method for adding or removing delay for a given Departure.
   */
  public void addDelay() {
    String trainNumTxt = showInputDialog("Find desired departure by train number:");
    int trainNum;
    while (trainNumTxt.isEmpty()) {
      trainNumTxt = showInputDialog("Train number cannot be empty");
    }
    try {
      trainNum = Integer.parseInt(trainNumTxt);
    } catch (Exception e) {
      trainNumTxt = showInputDialog("Train number must be a positive whole number");
    } finally {
      trainNum = Integer.parseInt(trainNumTxt);
    }

    String delayTxt = showInputDialog("Total delay in minutes (leave blank to remove delay):");
    int delay;
    if (delayTxt.isEmpty()) {
      delay = 0;
    } else {
      try {
        delay = Integer.parseInt(delayTxt);
      } catch (Exception e) {
        delayTxt = showInputDialog("Delay must be a positive whole number");
      } finally {
        delay = Integer.parseInt(delayTxt);
      }
    }

    dr.findDeparture(trainNum).setDelay(Duration.ofMinutes(delay));
    if (dr.findDeparture(trainNum).getDelay().equals(Duration.ofMinutes(0))) {
      showMessageDialog(null, "Delay removed.");
    } else {
      LocalTime del = LocalTime.of(dr.findDeparture(trainNum).getDelay().toHoursPart(),
              dr.findDeparture(trainNum).getDelay().toMinutesPart());
      showMessageDialog(null, "Delay is now " + del.format(DateTimeFormatter.ofPattern("h:m")));
    }
  }

  /**
   * Method for finding a specific departure given train number.
   */
  public void findDeparture() {
    String trainNumTxt = showInputDialog("Find desired departure by train number:");
    int trainNum;
    while (trainNumTxt.isEmpty()) {
      trainNumTxt = showInputDialog("Train number cannot be empty");
    }
    try {
      trainNum = Integer.parseInt(trainNumTxt);
    } catch (Exception e) {
      trainNumTxt = showInputDialog("Train number must be a positive whole number");
    } finally {
      trainNum = Integer.parseInt(trainNumTxt);
    }

    if (dr.findDeparture(trainNum) != null) {
      showMessageDialog(null,
              "  Time  | Line |Train Nr.| Destination    | Delay | New Time | Track |\n"
                      + dr.findDeparture(trainNum).toString());
    } else {
      showMessageDialog(null, "Departure not found");
    }
  }

  /**
   * Method for finding all departures heading to a specified destination.
   */
  public void findDestination() {
    String destination = showInputDialog("Destination:");
    while (destination.isEmpty()) {
      destination = showInputDialog("Destination cannot be empty");
    }

    if (dr.findDestination(destination) != null) {
      showMessageDialog(null, dr.findDestination(destination));
    } else {
      showMessageDialog(null, "Destination not found");
    }
  }

  /**
   * Method to update the time.
   */
  public void updateClock() {
    String newHourTxt = showInputDialog("Hour:");
    int hour;
    while (newHourTxt.isEmpty()) {
      newHourTxt = showInputDialog("Hour cannot be empty");
    }
    try {
      hour = Integer.parseInt(newHourTxt);
    } catch (Exception e) {
      newHourTxt = showInputDialog("Hour must be a positive whole number under 24,"
              + " and later on the day than " + time);
    } finally {
      hour = Integer.parseInt(newHourTxt);
    }
    while (hour < 0 || hour > 23) {
      newHourTxt = showInputDialog("Hour of departure must be between 0 and 24"
              + " and later on the day than " + time);
      hour = Integer.parseInt(newHourTxt);
    }
    while (time.getHour() > hour) {
      newHourTxt = showInputDialog("Hour must be later than " + time);
      hour = Integer.parseInt(newHourTxt);
    }

    String newMinTxt = showInputDialog("Minute:");
    int min;
    while (newMinTxt.isEmpty()) {
      newMinTxt = showInputDialog("Minute of departure cannot be empty");
    }
    try {
      min = Integer.parseInt(newMinTxt);
    } catch (Exception e) {
      newMinTxt = showInputDialog("Minute of departure must be a positive whole number under 60");
    } finally {
      min = Integer.parseInt(newMinTxt);
    }
    while (min < 0 || min > 59) {
      newMinTxt = showInputDialog("Minute of departure must be between 0 and 60");
      min = Integer.parseInt(newMinTxt);
    }

    if (hour == 0 && min == 0) {
      time = LocalTime.of(hour, min);
      showMessageDialog(null, "Time set to midnight");
    } else if (time.isBefore(LocalTime.of(hour, min))) {
      time = LocalTime.of(hour, min);
      showMessageDialog(null, "Time set to " + time.toString());
    } else {
      showMessageDialog(null, "Cannot go back in time. Set time to 00:00");
    }
  }
}




