import java.time.Duration;
import java.time.LocalTime;

/**
 * Test class DepartureRegister class
 */

class DepartureRegisterTest {
  public static void main(String[] args){
    //Initializing test counters and register
    int totalTests = 0; //amount of tests done
    int correct = 0; //amount of successful tests
    int wrong = 0; //amount of failed tests
    DepartureRegister r = new DepartureRegister();

    /*Testing addDeparture-methods*/
    //first addDeparture-method
    //positive
    try{
      totalTests++;
      if(r.addDeparture(LocalTime.of(9,45),"L12",1,11,"Eidsvoll", Duration.ofMinutes(37))) {
        r.addDeparture(LocalTime.of(9,45), "L12", 1, 11, "Eidsvoll", Duration.ofMinutes(37));
        correct++;
        System.out.println(correct + "/" + totalTests + " 1.1: addDeparture 1 O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 1.1: addDeparture 1 X   Expected: True, Got: " + r.addDeparture(LocalTime.of(9,45),"L12",1,11,"Eidsvoll",Duration.ofMinutes(37)));
      }
    }catch(Exception e){
      System.out.println("Error in addDeparture-method");
    }

    //negative
    try{
      totalTests++;
      if(!r.addDeparture(LocalTime.of(0,1)," ",1,1," ", Duration.ofMinutes(1))){
        correct++;
        System.out.println(correct + "/" + totalTests + " 1.2: addDeparture 1 O");
      }else{
        wrong++;
        System.out.println(correct + "/" + totalTests + " 1.2: addDeparture 1 X   Expected: False, Got: " + r.addDeparture(LocalTime.of(0,1)," ",1,1," ", Duration.ofMinutes(1)));
      }
    } catch(Exception e){
      System.out.println("Error in addDeparture-method");
    }

    //second addDeparture-method
    //positive
    try{
      totalTests++;
      if(r.addDeparture(LocalTime.of(9,57),"L1",2,"Asker",Duration.ofMinutes(100))) {
        r.addDeparture(LocalTime.of(9,57), "L1", 2, "Asker", Duration.ofMinutes(100));
        correct++;
        System.out.println(correct + "/" + totalTests + " 1.3: addDeparture 2 O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 1.3: addDeparture 2 X   Expected: True, Got: " + r.addDeparture(LocalTime.of(9,57),"L1",2,"Asker",Duration.ofMinutes(100)));
      }
    }catch(Exception e){
      System.out.println("Error in addDeparture-method");
    }

    //negative
    try{
      totalTests++;
      if(!r.addDeparture(LocalTime.of(0,0)," ",2," ", Duration.ofMinutes(0))){
        correct++;
        System.out.println(correct + "/" + totalTests + " 1.4: addDeparture 2 O");
      }else{
        wrong++;
        System.out.println(correct + "/" + totalTests + " 1.4: addDeparture 2 X   Expected: False, Got: " + r.addDeparture(LocalTime.of(0,0)," ",2," ", Duration.ofMinutes(0)));
      }
    } catch(Exception e){
      System.out.println("Error in addDeparture-method");
    }

    //third addDeparture-method
    //positive
    try{
      totalTests++;
      if(r.addDeparture(LocalTime.of(10,10),"F1x",3,13,"Oslo Lufthavn")) {
        r.addDeparture(LocalTime.of(10,10), "F1x", 3, 13, "Oslo Lufthavn");
        correct++;
        System.out.println(correct + "/" + totalTests + " 1.5: addDeparture 3 O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 1.5: addDeparture 3 X   Expected: True, Got: " + r.addDeparture(LocalTime.of(10,10),"F1x",3,13,"Oslo Lufthavn"));
      }
    }catch(Exception e){
      System.out.println("Error in addDeparture-method");
    }

    //negative
    //negative
    try{
      totalTests++;
      if(!r.addDeparture(LocalTime.of(0,0)," ",3,1," ")){
        correct++;
        System.out.println(correct + "/" + totalTests + " 1.6: addDeparture 3 O");
      }else{
        wrong++;
        System.out.println(correct + "/" + totalTests + " 1.6: addDeparture 3 X   Expected: False, Got: " + r.addDeparture(LocalTime.of(0,0)," ",1,3, " "));
      }
    } catch(Exception e){
      System.out.println("Error in addDeparture-method");
    }

    //fourth addDeparture-method
    //positive
    try{
      totalTests++;
      if(r.addDeparture(LocalTime.of(10,14),"L13",4,"Lillestrøm")) {
        r.addDeparture(LocalTime.of(10,14), "L13", 4, "Lillestrøm");
        correct++;
        System.out.println(correct + "/" + totalTests + " 1.7: addDeparture 4 O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 1.7: addDeparture 4 X   Expected: True, Got: " + r.addDeparture(LocalTime.of(10,14),"L13",4,"Lillestrøm"));
      }
    }catch(Exception e){
      System.out.println("Error in addDeparture-method");
    }

    //negative
    try{
      totalTests++;
      if(!r.addDeparture(LocalTime.of(0,0)," ",4," ")){
        correct++;
        System.out.println(correct + "/" + totalTests + " 1.8: addDeparture 4 O");
      }else{
        wrong++;
        System.out.println(correct + "/" + totalTests + " 1.8: addDeparture 4 X   Expected: False, Got: " + r.addDeparture(LocalTime.of(0,0)," ",4," "));
      }
    } catch(Exception e){
      System.out.println("Error in addDeparture-method");
    }

    if(correct == totalTests){
      System.out.println("addDeparture-methods working as expected\n");
    } else {
      System.out.println("Error in addDeparture-method(s)\n");
    }

    /*Testing findDeparture-method*/
    //positive test
    try{
      totalTests++;
      if(r.findDeparture(1)!=null){
        correct++;
        System.out.println(correct + "/" + totalTests + " 2.1: findDeparture O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 2.1: findDeparture X   Expected: TrainDeparture object, Got: null");
      }
    }catch(Exception e){
      System.out.println("Error in findDeparture-method");
    }

    //initializing TrainDeparture with same train number
    //positive test
    try{
      totalTests++;
      TrainDeparture equalFirst = new TrainDeparture(LocalTime.of(0,0)," ", 1, " ");
      if(r.findDeparture(1).equals(equalFirst)){
        correct++;
        System.out.println(correct + "/" + totalTests + " 2.2: findDeparture O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 2.2: findDeparture X   Expected: True, Got: " + r.findDeparture(1).equals(equalFirst));
      }
    }catch(Exception e){
      System.out.println("Error in findDeparture-method");
    }

    if (correct == totalTests) {
      System.out.println("findDeparture-method working as expected\n");
    } else {
      System.out.println("Error in findDeparture-method\n");
    }

    /*Testing findDestination-method*/
    //positive
    try{
      totalTests++;
      if(r.findDestination("Lillestrøm")!=null){
        correct++;
        System.out.println(correct + "/" + totalTests + " 3.1: findDestination O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 3.1: findDestination X   Expected: Timetable, Got: null");
      }
    } catch(Exception e){
      System.out.println("Error in findDestination-method");
    }

    r.addDeparture(LocalTime.of(16,0),"A4",5,"Lillestrøm");
    r.addDeparture(LocalTime.of(16,30),"A4",6,"Lillestrøm");
    r.addDeparture(LocalTime.of(17,0),"A4",7,"Lillestrøm");

    try{
      totalTests++;
      if(r.findDestination("Lillestrøm")!=null){
        correct++;
        System.out.println(correct + "/" + totalTests + " 3.2: findDestination O");
        System.out.println(r.findDestination("Lillestrøm"));
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 3.2: findDestination X   Expected: Timetable, Got: null");
      }
    } catch(Exception e){
      System.out.println("Error in findDestination-method");
    }

    if(correct==totalTests){
      System.out.println("findDestination-method working as expected\n");
    } else {
      System.out.println("Error in findDestination-method\n");
    }

    /*Visual tests: tests which require visual confirmation for working correctly*/

    System.out.println("Expected: only train 1 & 2 present on timetable");
    System.out.println(r.viewTable(LocalTime.of(10,20)));

    System.out.println("\nExpected: all trains present");
    System.out.println(r.viewTable(LocalTime.of(0,0)));

    System.out.println("\nExpected: trains 1 & 2 & 4 present");
    System.out.println(r.viewTable(LocalTime.of(10,11)));

    /*Testing removeDeparture-method*/
    //positive
    try{
      totalTests++;
      if(r.removeDeparture(r.findDeparture(1))==1){
        correct++;
        System.out.println("\n" + correct + "/" + totalTests + " 4.1: removeDeparture O");
      } else {
        wrong++;
        System.out.println("\n" + correct + "/" + totalTests + " 4.1: removeDeparture X   Expected: 1, Got: " + r.removeDeparture(r.findDeparture(1)));
      }

    }catch(Exception e){
      System.out.println("\nError in removeDeparture-method");
    }

    //visual
    System.out.println("\nExpected: train 1 removed");
    System.out.println(r.viewTable(LocalTime.of(0,0)));

    System.out.println( correct + "/" + totalTests + " correct, " + wrong + "/" + totalTests + " wrong");
  }
}
