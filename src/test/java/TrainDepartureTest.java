import java.time.Duration;
import java.time.LocalTime;

/**
 * Test class for TrainDeparture class
 */
class TrainDepartureTest {
  public static void main(String[] args){
    //Initializing test counters
    int totalTests = 0; //amount of tests done
    int correct = 0; //amount of successful tests
    int wrong = 0; //amount of failed tests

    /* Testing constructors and accessor-methods */
    /* Testing the first constructor */
    TrainDeparture train001 = new TrainDeparture(LocalTime.of(16,0),"A1",1,1,"Stavanger", Duration.ofMinutes(30));
    try{
      //Testing original departure time
      //positive test
      totalTests++;
      if(train001.getDepartureTime().equals(LocalTime.of(16,0))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 1.1: constructor 1 departure time O");
      }else{
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 1.1: constructor 1 departure time X   Expected: 1600, Got: " + train001.getDepartureTime());
      }

      //Testing new departure time (accounting for delay)
      //positive test
      totalTests++;
      if(train001.newTime().equals(LocalTime.of(16,30))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 1.2: constructor 1 new time O");
      }else{
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 1.2: constructor 1 new time X    Expected: 1630 " +  ", Got: " + train001.newTime());
      }

      //negative test
      totalTests++;
      if(!train001.newTime().equals(LocalTime.of(16,0))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 1.3: constructor 1 new time O");
      }else{
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 1.3: constructor 1 new time X    Expected: 1630, Got: " + train001.newTime());
      }

      //Testing line
      //positive test
      totalTests++;
      if(train001.getLine().equalsIgnoreCase("A1")){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 1.4: constructor 1 line O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 1.4: constructor 1 line X   Expected: A1, Got: " + train001.getLine());
      }

      //Testing train number
      //positive test
      totalTests++;
      if(train001.getTrainNumber() == 1){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 1.5: constructor 1 train number O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 1.5: constructor 1 train number X   Expected: 1, Got: " + train001.getTrainNumber());
      }

      //Testing track
      totalTests++;
      if(train001.getTrack() == 1){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 1.6: constructor 1 track O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 1.6: constructor 1 track X   Expected: 1, Got: " + train001.getTrack());
      }

      //Testing train destination
      //positive test
      totalTests++;
      if(train001.getDestination().equals("Stavanger")){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 1.7: constructor 1 destination O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 1.7: constructor 1 destination X   Expected: Stavanger, Got: " + train001.getDestination());
      }

      //Testing delay
      //positive test
      totalTests++;
      if(train001.getDelay().equals(Duration.ofMinutes(30))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 1.8: constructor 1 delay O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 1.8: constructor 1 delay X   Expected: 30, Got: " + train001.getDelay());
      }

      if(correct == totalTests){
        System.out.println("Initializing train001 successful\n");
      } else {
       System.out.println("Error in first constructor\n");
      }

    }catch(Exception e){
      System.out.println("Error in constructor 1");
    }

    /* Testing the second constructor: no assigned track */
    TrainDeparture train002 = new TrainDeparture(LocalTime.of(17,50),"B1", 2,"Oslo",Duration.ofMinutes(10));
    try{
      //testing original departure time
      //positive test
      totalTests++;
      if(train002.getDepartureTime().equals(LocalTime.of(17,50))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 2.1: constructor 1 departure time O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 2.1: constructor 2 departure time X   Expected: 1750, Got: " + train002.getDepartureTime());
      }

      //testing new time
      //positive test
      totalTests++;
      if(train002.newTime().equals(LocalTime.of(18,0))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 2.2: constructor 2 new time O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 2.2: constructor 2 new time X   Expected: 1800, Got: " + train002.newTime());
      }

      //negative test
      totalTests++;
      if(!train002.newTime().equals(LocalTime.of(17,50))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 2.3: constructor 2 new time O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 2.3: constructor 2 new time X   Expected: 1800, Got: " + train002.newTime());
      }

      //testing line
      //positive test
      totalTests++;
      if(train002.getLine().equals("B1")){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 2.4: constructor 2 line O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 2.4: constructor 2 line X   Expected: B1, Got: " + train002.getLine());
      }

      //testing train number
      //positive test
      totalTests++;
      if(train002.getTrainNumber() == 2){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 2.5: constructor 2 train number O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 2.5: constructor 2 train number X   Expected: 2, Got: " + train002.getTrainNumber());
      }

      //testing track
      totalTests++;
      if(train002.getTrack() == -1){
        correct++;
        System.out.println(correct + "/" + totalTests + " Test 2.6: constructor 2 track O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 2.6: constructor 2 track X   Expected: -1, Got: " + train002.getTrack());
      }

      //testing destination
      //positive test
      totalTests++;
      if(train002.getDestination().equals("Oslo")){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 2.7: constructor 2 destination O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 2.7: constructor 2 destination X   Expected: Oslo, Got: " + train002.getDestination());
      }

      //testing delay
      //positive test
      totalTests++;
      if(train002.getDelay().equals(Duration.ofMinutes(10))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 2.8: constructor 2 delay O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 2.8: constructor 2 delay X   Expected: 10, Got: " + train002.getDelay());
      }

      if(correct == totalTests){
        System.out.println("Initializing train002 successful\n");
      } else {
        System.out.println("Error in second constructor\n");
      }

    }catch(Exception e){
      System.out.println("Error in constructor 2\n");
    }

    /* Testing the third constructor: no delay assigned */
    TrainDeparture train003 = new TrainDeparture(LocalTime.of(11,12),"C2",3,3,"Trondheim");
    try{
      //Testing departure time
      //positive
      totalTests++;
      if(train003.getDepartureTime().equals(LocalTime.of(11,12))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 3.1: constructor 3 departure time O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 3.1: constructor 3 departure time X   Expected: 1112, Got: " + train003.getDepartureTime());
      }

      //Testing new time
      //positive
      totalTests++;
      if(train003.newTime().equals(LocalTime.of(11,12))){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 3.2: constructor 3 new time O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 3.2: constructor 3 new time X   Expected: 1112, Got: " + train003.newTime());
      }

      //Testing line
      //positive
      totalTests++;
      if(train003.getLine().equals("C2")){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 3.3: constructor 3 line O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " Test 3.3: constructor 3 line X   Expected: C2, Got: " + train003.getLine());
      }

      //Testing train number
      //positive
      totalTests++;
      if(train003.getTrainNumber()==3){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 3.4: constructor 3 train number O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 3.4: constructor 3 train number X   Expected: 3, Got: " + train003.getTrainNumber());
      }

      //Testing track
      //positive
      totalTests++;
      if(train003.getTrack() == 3){
        correct++;
        System.out.println( correct + "/" + totalTests + " Test 3.5: constructor 3 track O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " Test 3.5: constructor 3 track X   Expected: 3, Got: " + train003.getTrack());
      }

      //Testing destination
      //positive
      totalTests++;
      if(train003.getDestination().equals("Trondheim")){
        correct++;
        System.out.println( correct + "/" + totalTests + " 3.6: constructor 3 destination O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 3.6: constructor 3 destination X   Expected: Trondheim, Got: " + train003.getDestination());
      }

      //Testing delay
      //positive
      totalTests++;
      if(train003.getDelay().equals(Duration.ofMinutes(0))){
        correct++;
        System.out.println( correct + "/" + totalTests + " 3.7: constructor 3 delay O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 3.7: constructor 3 delay X   Expected: 0, Got: " + train003.getDelay());
      }


      if(correct == totalTests){
        System.out.println("Initializing train003 successful\n");
      } else {
        System.out.println("Error in third constructor\n");
      }

    }catch(Exception e){
      System.out.println("Error in constructor 3\n");
    }

    /* Testing the fourth and last constructor: no track or delay assigned */
    TrainDeparture train004 = new TrainDeparture(LocalTime.of(8,25),"A4",4,"Kristiansand");
    try{
      //Testing departure time
      //positive
      totalTests++;
      if(train004.getDepartureTime().equals(LocalTime.of(8,25))){
        correct++;
        System.out.println( correct + "/" + totalTests + " 4.1: constructor 4 departure time O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 4.1: constructor 4 departure time X   Expected: 825 , Got: " + train004.getDepartureTime());
      }

      //Testing new time
      //positive
      totalTests++;
      if(train004.newTime().equals(LocalTime.of(8,25))){
        correct++;
        System.out.println( correct + "/" + totalTests + " 4.2: constructor 4 new time O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 4.2: constructor 4 new time X   Expected: 825, Got: " + train004.newTime());
      }

      //Testing line
      //positive
      totalTests++;
      if(train004.getLine().equals("A4")){
        correct++;
        System.out.println( correct + "/" + totalTests + " 4.3: constructor 4 line O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 4.3: constructor 4 line X   Expected: A4, Got: " + train004.getLine());
      }

      //Testing train number
      //positive
      totalTests++;
      if(train004.getTrainNumber() == 4){
        correct++;
        System.out.println( correct + "/" + totalTests + " 4.4: constructor 4 train number O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 4.4: constructor 4 train number X   Expected: 4, Got: " + train004.getTrainNumber());
      }

      //Testing track
      //positive
      totalTests++;
      if(train004.getTrack() == -1){
        correct++;
        System.out.println( correct + "/" + totalTests + " 4.5: constructor 4 track O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 4.5: constructor 4 track X   Expected: -1, Got: " + train004.getTrack());
      }

      //Testing destination
      //positive
      totalTests++;
      if(train004.getDestination().equals("Kristiansand")){
        correct++;
        System.out.println( correct + "/" + totalTests + " 4.6: constructor 4 destination O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 4.6: constructor 4 destination X   Expected: Kristiansand, Got: " + train004.getDestination());
      }

      //Testing delay
      //positive
      totalTests++;
      if(train004.getDelay().equals(Duration.ofMinutes(0))){
        correct++;
        System.out.println( correct + "/" + totalTests + " 4.7: constructor 4 delay O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 4.7: constructor 4 delay X   Expected: 0, Got: " + train004.getDelay());
      }

      if(correct == totalTests){
        System.out.println("Initializing train004 successful\n");
      } else {
        System.out.println("Error in fourth constructor\n");
      }

    }catch(Exception e){
      System.out.println("Error in constructor 4\n");
    }

    /* Testing methods: mutator-methods*/
    //Testing setTrack
    try{
      if(train001.setTrack(6)) {
        train001.setTrack(6);
        //positive
        totalTests++;
        if (train001.getTrack() == 6) {
          correct++;
          System.out.println(correct + "/" + totalTests + " 5.1: setTrack O");
        } else {
          wrong++;
          System.out.println(correct + "/" + totalTests + " 5.1: setTrack X   Expected: 6, Got: " + train001.getTrack());
        }

        //negative
        totalTests++;
        if (train001.getTrack() != 1) {
          correct++;
          System.out.println(correct + "/" + totalTests + " 5.2: setTrack O");
        } else {
          wrong++;
          System.out.println(correct + "/" + totalTests + " 5.2: setTrack X   Expected: 6, Got: " + train001.getTrack());
        }
      }

      totalTests++;
      if(!train001.setTrack(-2)){
        correct++;
        System.out.println(correct + "/" + totalTests + " 5.3: setTrack O");
      } else {
        wrong++;
        System.out.println(correct + "/" + totalTests + " 5.3: setTrack X   Expected: False, Got: " + train001.setTrack(-2));
      }

      if(correct == totalTests){
        System.out.println("Mutator-method setTrack working as expected\n");
      } else {
        System.out.println("Error in mutator-method setTrack\n");
      }
    }catch(Exception e){
      System.out.println("Error in mutator-method setTrack\n");
    }

    //Testing setDelay
    try{
      if(train001.setDelay(Duration.ofMinutes(54))){
        train001.setDelay(Duration.ofMinutes(54));
        //positive
        totalTests++;
        if(train001.getDelay().equals(Duration.ofMinutes(54))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.1: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.1: setDelay X   Expected: 54, Got: " + train001.getDelay());
        }

        //positive new time
        totalTests++;
        if(train001.newTime().equals(LocalTime.of(16,54))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.2: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.2: setDelay X   Expected: 1654, Got: " + train001.newTime());
        }

        //negative
        totalTests++;
        if(!train001.getDelay().equals(Duration.ofMinutes(30))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.3: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.3: setDelay X   Expected: 54, Got: " + train001.getDelay());
        }

        //negative new time
        totalTests++;
        if(!train001.newTime().equals(LocalTime.of(16,30))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.4: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.4: setDelay X   Expected: 1654, Got: " + train001.newTime());
        }
      }

      if(train001.setDelay(Duration.ofMinutes(140))){
        train001.setDelay(Duration.ofMinutes(140));
        //positive
        totalTests++;
        if(train001.getDelay().equals(Duration.ofMinutes(140))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.5: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.5: setDelay X   Expected: 140, Got: " + train001.getDelay());
        }

        //positive new time
        totalTests++;
        if(train001.newTime().equals(LocalTime.of(18,20))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.6: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.6: setDelay X   Expected: 1820, Got: " + train001.newTime());
        }

        //negative
        totalTests++;
        if(!train001.getDelay().equals(Duration.ofMinutes(54))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.7: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.7: setDelay X   Expected: 140, Got: " + train001.getDelay());
        }

        //negative new time
        totalTests++;
        if(!train001.newTime().equals(LocalTime.of(16,54))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.8: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.8: setDelay X   Expected: 1740, Got: " + train001.newTime());
        }

        //negative
        totalTests++;
        if(!train001.getDelay().equals(Duration.ofMinutes(30))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.9: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.9: setDelay X   Expected: 100, Got: " + train001.getDelay());
        }

        //negative new time
        totalTests++;
        if(!train001.newTime().equals(LocalTime.of(16,30))){
          correct++;
          System.out.println( correct + "/" + totalTests + " 6.10: setDelay O");
        } else {
          wrong++;
          System.out.println( correct + "/" + totalTests + " 6.10: setDelay X   Expected: 1740, Got: " + train001.newTime());
        }
      }

      if(correct == totalTests){
        System.out.println("Mutator-method setDelay working as expected\n");
      } else {
        System.out.println("Error in mutator-method setDelay\n");
      }

    }catch(Exception e){
      System.out.println("Error in mutator-method setDelay\n");
    }

    /* Testing methods: misc. methods */
    //testing numberForSwitch
    try{
      //positive
      totalTests++;
      if(train001.numberForSwitch() == 4){
        correct++;
        System.out.println( correct + "/" + totalTests + " 7.1: numberForSwitch O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 7.1: numberForSwitch X   Expected: 0, Got: " + train001.numberForSwitch());
      }

      //positive
      totalTests++;
      if(train002.numberForSwitch() == 2){
        correct++;
        System.out.println( correct + "/" + totalTests + " 7.2: numberForSwitch O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 7.2: numberForSwitch X   Expected: 2, Got: " + train001.numberForSwitch());
      }

      //positive
      totalTests++;
      if(train003.numberForSwitch() == 3){
        correct++;
        System.out.println( correct + "/" + totalTests + " 7.3: numberForSwitch O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 7.3: numberForSwitch X   Expected: 3, Got: " + train001.numberForSwitch());
      }

      //positive
      totalTests++;
      if(train004.numberForSwitch() == 1){
        correct++;
        System.out.println( correct + "/" + totalTests + " 7.4: numberForSwitch O");
      } else {
        wrong++;
        System.out.println( correct + "/" + totalTests + " 7.4: numberForSwitch X   Expected: 1, Got: " + train004.numberForSwitch());
      }

      if(correct == totalTests){
        System.out.println("Misc. method numberForSwitch working as expected\n");
      } else {
        System.out.println("Error in misc. methods numberForSwitch\n");
      }
    } catch(Exception e){
      System.out.println("Error in misc. methods numberForSwitch\n");
    }

    System.out.println(train001.toString());

    System.out.println("\nTot. success: " + correct + "/" + totalTests + "\nTot. failed: " + wrong + "/" + totalTests);
  }
}
